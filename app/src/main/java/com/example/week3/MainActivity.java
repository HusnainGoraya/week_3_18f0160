package com.example.week3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String semail = "f180160@nu.edu.pk";
        String sepass = "Pass@456";
        EditText email = findViewById(R.id.email);
        EditText pass = findViewById(R.id.password);
        Button login =findViewById(R.id.login);
        Intent home = new Intent(this, HomeScreen.class);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals(semail)&& pass.getText().toString().equals(sepass)) {
                    Toast.makeText(MainActivity.this,"Success! Logged In",Toast.LENGTH_SHORT).show();
                    startActivity(home);
                    finish();
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Your Email or Password does not match. Please Try Again.");
                    builder.setTitle("Failed!");
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }
    public void Signup(View view) {
        TextView tv= (TextView) findViewById(R.id.signup);

        //alter text of textview widget
        tv.setText("Not a Member? Sign up now.");
        //assign the textview forecolor
        tv.setTextColor(Color.GREEN);
    startActivity(new Intent(MainActivity.this,SignupScreen.class));

    }
}